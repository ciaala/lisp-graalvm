package lisp;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Engine;
import org.graalvm.polyglot.PolyglotException;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public final class LispLanguageMain {

    private static final String ID = lisp.LispLanguage.ID;

    /**
     * The main entry point.
     */
    public static void main(final String[] args) throws IOException {
        System.out.println("Command line of: '" + LispLanguageMain.ID + "'");
        System.out.println("Classpath: '" + LispLanguageMain.getClasspathString(LispLanguageMain.class) + "'");
        final String truffleClassPath = System.getProperty("truffle.class.path.append");
        if (StringUtils.isNotBlank(truffleClassPath)) {
            System.out.println("truffle.class.path.append: '" + truffleClassPath + "', and exists? " + Files.exists(Paths.get(truffleClassPath)));
        }
        System.out.println("WorkingDirectory: '" + SystemUtils.getUserDir() + "'");
        final Source source;
        final Map<String, String> options = new HashMap<>();
        String file = null;
        for (final String arg : args) {
            System.out.println("#" + arg);
            if (LispLanguageMain.parseOption(options, arg)) {
                continue;
            } else {
                if (file == null) {
                    file = arg;
                }
            }
        }

        if (file == null) {
            // @formatter:off
            source = Source.newBuilder(LispLanguageMain.ID, new InputStreamReader(System.in), "<stdin>").build();
            // @formatter:on
        } else {
            System.out.println("Using " + file + " as program source.");
            source = Source.newBuilder(LispLanguageMain.ID, new File(file)).build();
        }

        final int exitCode = LispLanguageMain.executeSource(source, System.in, System.out, options);
        System.exit(exitCode);

    }

    private static int executeSource(final Source source, final InputStream in, final PrintStream out, final Map<String, String> options) {
        final Context context;
        final PrintStream err = System.err;
        try {
            context = Context.newBuilder(LispLanguageMain.ID).in(in).out(out).options(options).build();
        } catch (final IllegalArgumentException e) {
            err.println(e.getMessage());
            return 1;
        }
        final Engine engine = context.getEngine();
        out.println("== running on: " + engine.getImplementationName() + " version: " + engine.getVersion());

        try {
            final Value result = context.eval(source);
            if (context.getBindings(LispLanguageMain.ID).getMember("main") == null) {
                err.println("No function main() defined in SL source file.");
                return 1;
            }
            if (!result.isNull()) {
                out.println(result.toString());
            }
            return 0;
        } catch (final PolyglotException ex) {
            if (ex.isInternalError()) {
                // for internal errors we print the full stack trace
                ex.printStackTrace();
            } else {
                err.println(ex.getMessage());
            }
            return 1;
        } finally {
            context.close();
        }
    }

    private static boolean parseOption(final Map<String, String> options, final String arg) {

        if (arg.length() <= 2 || !arg.startsWith("--")) {
            return false;
        }
        final int eqIdx = arg.indexOf('=');
        final String key;
        String value;
        if (eqIdx < 0) {
            key = arg.substring(2);
            value = null;
        } else {
            key = arg.substring(2, eqIdx);
            value = arg.substring(eqIdx + 1);
        }

        if (value == null) {
            value = "true";
        }
        final int index = key.indexOf('.');
        String group = key;
        if (index >= 0) {
            group = group.substring(0, index);
        }
        options.put(key, value);
        return true;
    }

    public static String getClasspathString(final Class<LispLanguageMain> aClass) {
        final StringBuilder classpath = new StringBuilder();
        ClassLoader applicationClassLoader = aClass.getClassLoader();
        if (applicationClassLoader == null) {
            applicationClassLoader = ClassLoader.getSystemClassLoader();
        }
        final URL[] urls = ((URLClassLoader) applicationClassLoader).getURLs();
        for (int i = 0; i < urls.length; i++) {
            classpath.append(urls[i].getFile()).append("\r\n");
        }
        return classpath.toString();
    }
}
