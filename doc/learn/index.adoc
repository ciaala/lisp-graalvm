= Learning Path

The best approach to learn how to use the combination of the tools to generate a DSL on top of GraalVM is to start from the grammar for a language.

== Path suggestion 2020-04

=== Language to Grammar

- Language: Lisp
- SExpression Grammar in ANTLR4: Lisp Grammar (parser and lexer)
- Verify the grammar output: the AST of Lisp

=== Feeding Truffle

- The truffle Nodes
- Generation of Truffle Nodes
- Looking at the Truffle Nodes

=== Runtime Support

- Runtime Overview: Nodes Builtin
- Builtin PRINT, READ: the least amount of runtime to reach an example
- Control Nodes: If, Loop

=== Execution

- Running some sample snippets against your implementation
- Measuring the performance
- Check the performance of the original implementation of your DSL

=== Integration

- The polyglot binding in your native runtime