package lisp;

import com.oracle.truffle.api.source.Source;
import lisp.parser.BailoutErrorListener;
import lisp.parser.LispParserVisitor;
import lisp.runtime.LispLanguageExpressionRootNode;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

public class LispLanguageParserUtility {
    public static LispLanguageExpressionRootNode parse(final LispLanguage language, final Source source) {

        final LispLanguageLexer lexer = new LispLanguageLexer(CharStreams.fromString(source.getCharacters().toString()));
        final LispLanguageParser parser = new LispLanguageParser(new CommonTokenStream(lexer));
        lexer.removeErrorListeners();
        parser.removeErrorListeners();
        final BailoutErrorListener listener = new BailoutErrorListener(source);
        lexer.addErrorListener(listener);
        parser.addErrorListener(listener);
        final LispParserVisitor lispParserVisitor = new LispParserVisitor(language, source);
        parser.addParseListener(lispParserVisitor);
        parser.lispLanguage();

        return lispParserVisitor.getRootExpression();
    }
}
