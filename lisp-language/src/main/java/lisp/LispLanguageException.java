package lisp;

import com.oracle.truffle.api.TruffleException;
import com.oracle.truffle.api.nodes.Node;

public class LispLanguageException extends RuntimeException implements TruffleException {
    private final Node location;

    public LispLanguageException(final String message,
                                 final Node location) {
        super(message);
        this.location = location;
    }

    @Override
    public Node getLocation() {
        return this.location;
    }
}
