package lisp.parser;

import com.oracle.truffle.api.source.Source;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;

public class BailoutErrorListener extends BaseErrorListener {


    private final Source source;

    public BailoutErrorListener(final Source source) {
        this.source = source;
    }

    private static void throwParseError(final Source source, final int line, final int charPositionInLine, final Token token, final String message) {
        final int col = charPositionInLine + 1;
        final String location = "-- line " + line + " col " + col + ": ";
        final int length = token == null ? 1 : Math.max(token.getStopIndex() - token.getStartIndex(), 0);
        final String formattedMessage = String.format("Error(s) parsing script:%n" + location + message);
        throw new LispLanguageParseError(source, line, col, length, formattedMessage);
    }

    @Override
    public void syntaxError(final Recognizer<?, ?> recognizer, final Object offendingSymbol, final int line, final int charPositionInLine, final String msg, final RecognitionException e) {
        BailoutErrorListener.throwParseError(this.source, line, charPositionInLine, (Token) offendingSymbol, msg);
    }
}
