package lisp.parser;

import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.api.source.Source;
import lisp.LispLanguage;

import java.util.HashMap;
import java.util.Map;

public class LispLanguageNodeFactory {

    private final LispLanguage language;
    private final Source source;
    private final Map<String, RootCallTarget> allFunctions;

    public LispLanguageNodeFactory(final LispLanguage language, final Source source) {
        this.language = language;
        this.source = source;
        this.allFunctions = new HashMap<>();
    }

}
