package lisp.parser;

import com.oracle.truffle.api.TruffleException;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.source.SourceSection;

public class LispLanguageParseError extends RuntimeException implements TruffleException {
    private final Source source;
    private final int line;
    private final int column;
    private final int length;
    private final String format;

    public LispLanguageParseError(final Source source,
                                  final int line,
                                  final int column,
                                  final int length,
                                  final String message) {
        super(message);
        this.source = source;
        this.line = line;
        this.column = column;
        this.length = length;
        this.format = message;
    }

    @Override
    public SourceSection getSourceLocation() {
        return this.source.createSection(this.line, this.column, this.length);
    }

    @Override
    public Node getLocation() {
        return null;
    }

    @Override
    public boolean isSyntaxError() {
        return true;
    }
}
