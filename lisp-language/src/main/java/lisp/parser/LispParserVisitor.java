package lisp.parser;

import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.source.Source;
import lisp.LispLanguage;
import lisp.LispLanguageListener;
import lisp.LispLanguageParser;
import lisp.runtime.LispLanguageBigDecimalNode;
import lisp.runtime.LispLanguageExpressionNode;
import lisp.runtime.LispLanguageExpressionRootNode;
import lisp.runtime.LispLanguageIdentifierNode;
import lisp.runtime.LispLanguageStringLiteral;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.util.ArrayDeque;

public class LispParserVisitor implements LispLanguageListener {
    private final LispLanguage lispLanguage;
    private final LispLanguageExpressionRootNode root;
    private final ArrayDeque<LispLanguageExpressionNode> stack = new ArrayDeque<>();
    private final Source source;

    public LispParserVisitor(final LispLanguage lispLanguage,
                             final Source source) {
        this.source = source;
        this.lispLanguage = lispLanguage;
        this.root = new LispLanguageExpressionRootNode(lispLanguage, new FrameDescriptor());
        this.stack.add(this.root.getBody());
    }

    @Override
    public void enterLispLanguage(final LispLanguageParser.LispLanguageContext ctx) {
        System.out.println(this.root.toString());
        System.out.println("=Visitor=========");
        this.printRule("enterLispLanguage", ctx);
    }

    @Override
    public void exitLispLanguage(final LispLanguageParser.LispLanguageContext ctx) {
        System.out.println("=Visitor=========");
        System.out.println(this.root.toString());
    }

    @Override
    public void enterSExpression(final LispLanguageParser.SExpressionContext ctx) {
        this.printRule("enterSExpression", ctx);
        final LispLanguageExpressionNode lispLanguageExpressionNode = new LispLanguageExpressionNode();
        this.stack.peek().addChild(lispLanguageExpressionNode);
        this.stack.push(lispLanguageExpressionNode);
    }

    @Override
    public void exitSExpression(final LispLanguageParser.SExpressionContext ctx) {
        this.stack.pop();
    }

    @Override
    public void enterIdentifier(final LispLanguageParser.IdentifierContext ctx) {
        this.printRule("enterIdentifier", ctx);
        this.stackCannotBeEmpty(ctx);
        this.stack.peek().addChild(new LispLanguageIdentifierNode(this.getText(ctx)));
    }

    @Override
    public void exitIdentifier(final LispLanguageParser.IdentifierContext ctx) {

    }

    @Override
    public void enterStringLiteral(final LispLanguageParser.StringLiteralContext ctx) {
        this.printRule("enterStringLiteral", ctx);
        this.stackCannotBeEmpty(ctx);
        this.stack.peek().addChild(new LispLanguageStringLiteral(this.getText(ctx)));
    }

    private void printRule(final String s, final ParserRuleContext prc) {
        System.out.println("Visitor " + s + " :" + this.getText(prc));
    }

    private String getText(final ParserRuleContext prc) {
        final int startIndex = prc.start.getStartIndex();
        final int stopIndex = prc.start.getStopIndex();
        final Interval sourceInterval = new Interval(startIndex, stopIndex);
        return prc.start.getInputStream().getText(sourceInterval);
    }

    @Override
    public void exitStringLiteral(final LispLanguageParser.StringLiteralContext ctx) {

    }

    @Override
    public void enterNumericLiteral(final LispLanguageParser.NumericLiteralContext ctx) {
        this.printRule("enterNumericLiteral", ctx);
        this.stackCannotBeEmpty(ctx);
        final BigDecimal bigDecimal = NumberUtils.createBigDecimal(this.getText(ctx));
        this.stack.peek().addChild(new LispLanguageBigDecimalNode(bigDecimal));
    }

    @Override
    public void exitNumericLiteral(final LispLanguageParser.NumericLiteralContext ctx) {

    }

    // GENERIC Listener

    @Override
    public void visitTerminal(final TerminalNode node) {

    }

    @Override
    public void visitErrorNode(final ErrorNode node) {

    }

    @Override
    public void enterEveryRule(final ParserRuleContext ctx) {

    }

    @Override
    public void exitEveryRule(final ParserRuleContext ctx) {

    }

    public LispLanguageExpressionRootNode getRootExpression() {
        return this.root;
    }


    private void stackCannotBeEmpty(final ParserRuleContext ctx) {
        if (this.stack.isEmpty()) {
            // TODO push a meaningful exception
            // ctx.exception = new RecognitionException("Unexpected");
            System.err.println(ctx);
            throw new RuntimeException("Stack is empty");
        }
    }
}
