package lisp;

import com.oracle.truffle.api.TruffleFile;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.charset.Charset;

public class LispLanguageFileDetector implements TruffleFile.FileTypeDetector {
    @Override
    public String findMimeType(TruffleFile file) throws IOException {
        return StringUtils.endsWith(file.getName(), ".lisp") ? LispLanguage.MIME_TYPE : null;
    }

    @Override
    public Charset findEncoding(TruffleFile file) throws IOException {
        return null;
    }
}
