package lisp;

import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.TruffleLanguage;
import com.oracle.truffle.api.TruffleLanguage.ContextPolicy;
import com.oracle.truffle.api.debug.DebuggerTags;
import com.oracle.truffle.api.dsl.NodeFactory;
import com.oracle.truffle.api.instrumentation.ProvidedTags;
import com.oracle.truffle.api.instrumentation.StandardTags;
import com.oracle.truffle.api.interop.InteropLibrary;
import com.oracle.truffle.api.source.Source;
import lisp.runtime.LispLanguageContext;
import lisp.runtime.LispLanguageExpressionRootNode;
import lisp.runtime.builtin.LispLanguageBuiltinNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@TruffleLanguage.Registration(
        id = LispLanguage.ID,
        name = LispLanguage.ID,
        defaultMimeType = LispLanguage.MIME_TYPE,
        characterMimeTypes = LispLanguage.MIME_TYPE,
        contextPolicy = ContextPolicy.SHARED,
        fileTypeDetectors = LispLanguageFileDetector.class,
        version = "0.1"
)
@ProvidedTags(value = {
        StandardTags.CallTag.class,
        StandardTags.StatementTag.class,
        StandardTags.RootTag.class,
        StandardTags.RootBodyTag.class,
        StandardTags.ExpressionTag.class,
        DebuggerTags.AlwaysHalt.class,
        StandardTags.ReadVariableTag.class,
        StandardTags.WriteVariableTag.class
})

public final class LispLanguage extends TruffleLanguage<LispLanguageContext> {
    public static final String ID = "LispLanguage";
    public static final String MIME_TYPE = "application/x-lisp";
    private static final List<NodeFactory<? extends LispLanguageBuiltinNode>> EXTERNAL_BUILTINS
            = Collections.synchronizedList(new ArrayList<>());
    private static final AtomicInteger counter = new AtomicInteger();
    private final int instanceSequenceNumber;

    public LispLanguage() {
        this.instanceSequenceNumber = LispLanguage.counter.incrementAndGet();
        System.out.println("LanguageInterpreter has been created: " + LispLanguage.counter);
    }

    @Override
    protected LispLanguageContext createContext(final Env env) {
        final ArrayList<NodeFactory<? extends LispLanguageBuiltinNode>> externalBuiltins = new ArrayList<>(LispLanguage.EXTERNAL_BUILTINS);
        return new LispLanguageContext(this, env, externalBuiltins);
    }

    @Override
    protected boolean isObjectOfLanguage(final Object object) {
        return false;
    }

    @Override
    protected boolean isVisible(final LispLanguageContext context, final Object value) {
        return !InteropLibrary.getFactory().getUncached(value).isNull(value);
    }

    public int getInstanceSequenceNumber() {
        return this.instanceSequenceNumber;
    }

    @Override
    protected CallTarget parse(final ParsingRequest request) throws Exception {
        final Source source = request.getSource();
        //final Map<String, RootCallTarget> functions;
        // final List<LispLanguageExpressionNode> topExpressions;
        /*
         * Parse the provided source. At this point, we do not have a SLContext yet. Registration of
         * the functions with the SLContext happens lazily in SLEvalRootNode.
         */

        final LispLanguageExpressionRootNode topExpression = LispLanguageParserUtility.parse(this, source);


        assert (topExpression != null);
        return Truffle.getRuntime().createCallTarget(topExpression);
    }
}
