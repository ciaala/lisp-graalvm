package lisp.runtime;

import com.oracle.truffle.api.nodes.Node;
import lisp.LispLanguageException;

public class LispLanguageUndefinedNameException extends LispLanguageException {
    public LispLanguageUndefinedNameException(final String message, final Node location) {
        super(message, location);
    }

    public static LispLanguageUndefinedNameException undefinedFunction(final Node location, final String name) {
        return new LispLanguageUndefinedNameException("Undefined function: " + name, location);
    }

    public static LispLanguageUndefinedNameException undefinedSymbol(final Node location, final String name) {
        return new LispLanguageUndefinedNameException("Undefined property: " + name, location);
    }
}
