package lisp.runtime;

import com.oracle.truffle.api.RootCallTarget;
import lisp.LispLanguage;

import java.util.Map;

public final class LispLanguageFunctionRegistry {
    private final LispLanguage language;
    private final LispFunctionsObject functionsObject = new LispFunctionsObject();

    public LispLanguageFunctionRegistry(final LispLanguage language) {
        this.language = language;
    }

    public LispLanguageFunction lookup(final String name, final boolean createIfNotPresent) {
        LispLanguageFunction result = this.functionsObject.functions.get(name);
        if (result == null && createIfNotPresent) {
            result = new LispLanguageFunction(this.language, name);
            this.functionsObject.functions.put(name, result);
        }
        return result;
    }

    /**
     * Associates the {@link LispLanguageFunction} with the given name with the given implementation root
     * node. If the function did not exist before, it defines the function. If the function existed
     * before, it redefines the function and the old implementation is discarded.
     */
    public LispLanguageFunction register(final String name, final RootCallTarget callTarget) {
        final LispLanguageFunction function = this.lookup(name, true);
        function.setCallTarget(callTarget);
        return function;
    }

    public void register(final Map<String, RootCallTarget> newFunctions) {
        for (final Map.Entry<String, RootCallTarget> entry : newFunctions.entrySet()) {
            this.register(entry.getKey(), entry.getValue());
        }
    }
}
