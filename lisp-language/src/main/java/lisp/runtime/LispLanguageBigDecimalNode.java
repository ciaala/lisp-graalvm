package lisp.runtime;

import com.oracle.truffle.api.frame.VirtualFrame;

import java.math.BigDecimal;

public class LispLanguageBigDecimalNode extends LispLanguageExpressionNode {
    private final BigDecimal value;

    public LispLanguageBigDecimalNode(final BigDecimal value) {
        this.value = value;
    }

    @Override
    public Object execute(final VirtualFrame frame) {
        return this.value;
    }
}
