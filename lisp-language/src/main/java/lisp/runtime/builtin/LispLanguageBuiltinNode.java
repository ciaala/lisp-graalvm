package lisp.runtime.builtin;

import com.oracle.truffle.api.dsl.GenerateNodeFactory;
import com.oracle.truffle.api.dsl.NodeChild;
import lisp.runtime.LispLanguageExpressionNode;

@NodeChild(value = "arguments", type = LispLanguageExpressionNode[].class)
@GenerateNodeFactory
public abstract class LispLanguageBuiltinNode extends LispLanguageExpressionNode {

}
