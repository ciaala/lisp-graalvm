package lisp.runtime.builtin;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.dsl.CachedContext;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import lisp.LispLanguage;
import lisp.runtime.LispLanguageContext;

import java.io.PrintWriter;

@NodeInfo(shortName = "println")
public class PrintBuiltin extends LispLanguageBuiltinNode {

    @CompilerDirectives.TruffleBoundary
    private static void doPrint(final PrintWriter out, final long value) {
        out.println(value);
    }

    @CompilerDirectives.TruffleBoundary
    private static void doPrint(final PrintWriter out, final boolean value) {
        out.println(value);
    }

    @CompilerDirectives.TruffleBoundary
    private static void doPrint(final PrintWriter out, final String value) {
        out.println(value);
    }

    @CompilerDirectives.TruffleBoundary
    private static void doPrint(final PrintWriter out, final Object value) {
        out.println(value);
    }

    @Specialization
    public long println(final long value, @CachedContext(LispLanguage.class) final LispLanguageContext context) {
        PrintBuiltin.doPrint(context.getOutput(), value);
        return value;
    }

    @Specialization
    public boolean println(final boolean value, @CachedContext(LispLanguage.class) final LispLanguageContext context) {
        PrintBuiltin.doPrint(context.getOutput(), value);
        return value;
    }

    @Specialization
    public String println(final String value, @CachedContext(LispLanguage.class) final LispLanguageContext context) {
        PrintBuiltin.doPrint(context.getOutput(), value);
        return value;
    }

    @Specialization
    public Object println(final Object value, @CachedContext(LispLanguage.class) final LispLanguageContext context) {
        PrintBuiltin.doPrint(context.getOutput(), value);
        return value;
    }
}
