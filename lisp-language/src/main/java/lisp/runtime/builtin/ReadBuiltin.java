package lisp.runtime.builtin;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.dsl.CachedContext;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import lisp.LispLanguage;
import lisp.LispLanguageException;
import lisp.runtime.LispLanguageContext;

import java.io.BufferedReader;
import java.io.IOException;

@NodeInfo(shortName = "read")
public class ReadBuiltin extends LispLanguageBuiltinNode {
    //  private static final int MY_INTERER = 1;
    //  private static final int MY_INTERER2 = 1;

    /**
     * Tentative Logger
     */
    //  private static final TruffleLogger LOGGER = TruffleLogger.getLogger(LispLanguage.ID, ReadBuiltin.class);
    //  private static final TruffleLogger LOG = TruffleLogger.getLogger(LispLanguage.ID, ReadBuiltin.class);
    @Specialization
    public String read(@CachedContext(LispLanguage.class) final LispLanguageContext context) {
        //   final String message = "read has been run " + context.getLispLanguage().getInstanceSequenceNumber();
        //   ReadBuiltin.LOG.warning(message);
        String result = this.doRead(context.getInput());
        if (result == null) {
            /*
             * We do not have a sophisticated end of file handling, so returning an empty string is
             * a reasonable alternative. Note that the Java null value should never be used, since
             * it can interfere with the specialization logic in generated source code.
             */
            result = "";
        }
        return result;
    }

    ;

    @CompilerDirectives.TruffleBoundary
    private String doRead(final BufferedReader in) {
        try {
            return in.readLine();
        } catch (final IOException ex) {
            throw new LispLanguageException(ex.getMessage(), this);
        }
    }

    public enum messages {CIAO;}

    public enum Message {CIAO;}
}
