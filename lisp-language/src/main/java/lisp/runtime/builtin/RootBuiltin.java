package lisp.runtime.builtin;

import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.VirtualFrame;
import lisp.runtime.LispLanguageExpressionNode;
import lisp.runtime.LispLanguageNull;

// TODO delete it

public class RootBuiltin extends LispLanguageExpressionNode implements RootCallTarget {

    @Override
    public Object execute(final VirtualFrame frame) {
        final Object result = null;
        // this.getChildren().forEach( expression -> expression.execute(frame));

        return new Integer(0);
    }

    @Override
    public Object call(final Object... arguments) {
        final VirtualFrame virtualFrame = Truffle.getRuntime().createVirtualFrame(arguments, new FrameDescriptor());
        this.execute(virtualFrame);
        return LispLanguageNull.SINGLETON;
    }
}
