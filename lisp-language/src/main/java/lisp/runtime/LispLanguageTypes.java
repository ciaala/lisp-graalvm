package lisp.runtime;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.dsl.ImplicitCast;
import com.oracle.truffle.api.dsl.TypeCast;
import com.oracle.truffle.api.dsl.TypeCheck;
import com.oracle.truffle.api.dsl.TypeSystem;

import java.math.BigDecimal;

@TypeSystem({long.class, boolean.class})
public abstract class LispLanguageTypes {

    @ImplicitCast
    @CompilerDirectives.TruffleBoundary
    public static LispLanguageBigDecimalNode castBigDecimal(final long value) {
        return new LispLanguageBigDecimalNode(BigDecimal.valueOf(value));
    }

    @TypeCast(LispLanguageNull.class)
    public static LispLanguageNull asNull(final Object value) {
        assert LispLanguageTypes.isNull(value);
        return LispLanguageNull.SINGLETON;
    }

    @TypeCheck(LispLanguageNull.class)
    public static boolean isNull(final Object value) {
        return value == LispLanguageNull.SINGLETON;
    }

}
