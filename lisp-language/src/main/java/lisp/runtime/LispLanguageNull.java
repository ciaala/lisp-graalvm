package lisp.runtime;

import com.oracle.truffle.api.interop.TruffleObject;

public class LispLanguageNull implements TruffleObject {

    public static final LispLanguageNull SINGLETON = new LispLanguageNull();

    @Override
    public String toString() {
        return "nil";
    }
}
