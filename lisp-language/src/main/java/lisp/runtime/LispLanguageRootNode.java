package lisp.runtime;

import com.oracle.truffle.api.TruffleLogger;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.RootNode;
import com.oracle.truffle.api.source.SourceSection;
import lisp.LispLanguage;
import org.apache.commons.lang3.StringUtils;

public class LispLanguageRootNode extends RootNode {
    private static final TruffleLogger LOGGER = TruffleLogger.getLogger(LispLanguage.ID, LispLanguageRootNode.class);

    private final LispLanguageExpressionNode bodyNode;
    private final SourceSection sourceSection;
    private final String name;

    public LispLanguageRootNode(final LispLanguage lispLanguage,
                                final FrameDescriptor frameDescriptor,
                                final LispLanguageExpressionNode bodyNode,
                                final SourceSection sourceSection,
                                final String name) {
        super(lispLanguage, frameDescriptor);
        this.bodyNode = bodyNode;
        this.sourceSection = sourceSection;
        this.name = name;
    }

    @Override
    public Object execute(final VirtualFrame frame) {
        assert this.lookupContextReference(LispLanguage.class).get() != null;

        final Object[] arguments = frame.getArguments();
        LispLanguageRootNode.LOGGER.fine("Logging before exception: " + StringUtils.join(" ", arguments));
        return this.bodyNode.execute(frame);

    }
}
