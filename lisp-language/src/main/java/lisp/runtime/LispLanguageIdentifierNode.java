package lisp.runtime;

import com.oracle.truffle.api.frame.VirtualFrame;

public class LispLanguageIdentifierNode extends LispLanguageExpressionNode {
    private final String identifier;

    public LispLanguageIdentifierNode(final String identifier) {
        this.identifier = identifier;
    }

    @Override
    public Object execute(final VirtualFrame frame) {

        return this.identifier;
    }
}
