package lisp.runtime;

import com.oracle.truffle.api.CompilerAsserts;
import com.oracle.truffle.api.dsl.ReportPolymorphism;
import com.oracle.truffle.api.dsl.TypeSystemReference;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.instrumentation.GenerateWrapper;
import com.oracle.truffle.api.instrumentation.InstrumentableNode;
import com.oracle.truffle.api.instrumentation.ProbeNode;
import com.oracle.truffle.api.instrumentation.StandardTags;
import com.oracle.truffle.api.instrumentation.Tag;
import com.oracle.truffle.api.interop.ArityException;
import com.oracle.truffle.api.interop.InteropLibrary;
import com.oracle.truffle.api.interop.UnsupportedMessageException;
import com.oracle.truffle.api.interop.UnsupportedTypeException;
import com.oracle.truffle.api.nodes.ExplodeLoop;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.NodeInfo;
import lisp.LispLanguage;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

@NodeInfo(language = "LispLanguage", description = "The S-expression node and it is the base node that produces a value")
@GenerateWrapper
@ReportPolymorphism
@TypeSystemReference(LispLanguageTypes.class)
public class LispLanguageExpressionNode extends Node implements InstrumentableNode {

    // Static
    private static final int NO_SOURCE = -1;
    private static final int UNAVAILABLE_SOURCE = -2;
    //
    // Truffle Children
    @Child
    private InteropLibrary library = InteropLibrary.getFactory().createDispatched(3);
    @Child
    private LispLanguageExpressionNode identifier;
    @Children
    private LispLanguageExpressionNode[] cons = null;
    //
    //
    //
    private boolean hasRootTag;
    private boolean hasStatementTag;
    private int sourceCharIndex;
    private int sourceLength;

    public boolean hasTag(final Class<? extends Tag> tag) {
        if (tag == StandardTags.StatementTag.class) {
            return this.hasStatementTag;
        } else if (tag == StandardTags.RootTag.class || tag == StandardTags.RootBodyTag.class) {
            return this.hasRootTag;
        }
        return false;
    }

    public final void addStatementTag() {
        this.hasStatementTag = true;
    }

    @Override
    public WrapperNode createWrapper(final ProbeNode probe) {
        return new LispLanguageWrapperNode(this, probe);
    }

    public void setHasRootTag(final boolean hasRootTag) {
        this.hasRootTag = hasRootTag;
    }

    public final void setSourceSection(final int charIndex, final int length) {
        assert this.sourceCharIndex == LispLanguageExpressionNode.NO_SOURCE : "source must only be set once";
        if (charIndex < 0) {
            throw new IllegalArgumentException("charIndex < 0");
        } else if (length < 0) {
            throw new IllegalArgumentException("length < 0");
        }
        this.sourceCharIndex = charIndex;
        this.sourceLength = length;
    }

    public final void setUnavailableSourceSection() {
        assert this.sourceCharIndex == LispLanguageExpressionNode.NO_SOURCE : "source must only be set once";
        this.sourceCharIndex = LispLanguageExpressionNode.UNAVAILABLE_SOURCE;
    }

    @ExplodeLoop
    public Object execute(final VirtualFrame frame) {

        // Identifier Value
        final Object function = this.identifier.execute(frame);

        // Argument Values
        final Object[] argumentValues;
        if (this.cons != null) {
            final int argumentSize = this.cons.length;
            CompilerAsserts.compilationConstant(argumentSize);
            argumentValues = new Object[argumentSize];

            for (int i = 0; i < argumentSize; i++) {
                final LispLanguageExpressionNode con = this.cons[i];
                argumentValues[i] = con.execute(frame);
            }
        } else {
            argumentValues = new Object[0];
        }

        // Invoke
        try {
            final LispLanguageFunction lookup = this.lookupContextReference(LispLanguage.class).get().getFunctionRegistry().lookup(function.toString(), false);

            return this.library.execute(function, argumentValues);
        } catch (final ArityException | UnsupportedMessageException | UnsupportedTypeException e) {
            throw LispLanguageUndefinedNameException.undefinedFunction(this, function.toString());
        }
    }

    public final boolean hasSource() {
        return this.sourceCharIndex != LispLanguageExpressionNode.NO_SOURCE;
    }

    @Override
    public boolean isInstrumentable() {
        return this.hasSource();
    }

    /**
     * Marks this node as being a {@link StandardTags.RootTag} and {@link StandardTags.RootBodyTag}
     * for instrumentation purposes.
     */
    public final void addRootTag() {
        this.hasRootTag = true;
    }

    public final int getSourceCharIndex() {
        return this.sourceCharIndex;
    }

    public final int getSourceEndIndex() {
        return this.sourceCharIndex + this.sourceLength;
    }

    public final int getSourceLength() {
        return this.sourceLength;
    }

    // TODO rewrite
    public final void addChild(final LispLanguageExpressionNode expressionNode) {
        if (this.identifier == null) {
            this.identifier = expressionNode;
        } else {
            if (this.cons == null) {
                this.cons = new LispLanguageExpressionNode[]{expressionNode};
            } else {
                this.cons = Arrays.copyOf(this.cons, this.cons.length + 1);
                this.cons[this.cons.length - 1] = expressionNode;
            }
        }
    }

    @Override
    public String toString() {
        return "(" + super.toString() + "|" + StringUtils.join(this.getChildren(), " ") + ")";
    }
}
