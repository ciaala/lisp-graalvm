package lisp.runtime;

import com.oracle.truffle.api.frame.VirtualFrame;

public class LispLanguageStringLiteral extends LispLanguageExpressionNode {
    private final String text;

    public LispLanguageStringLiteral(final String text) {
        this.text = text;
    }

    @Override
    public Object execute(final VirtualFrame frame) {
        return this.text;
    }

    @Override
    public boolean isInstrumentable() {
        return false;
    }
}
