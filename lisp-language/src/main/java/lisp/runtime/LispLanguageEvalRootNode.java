package lisp.runtime;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.DirectCallNode;
import com.oracle.truffle.api.nodes.RootNode;
import lisp.LispLanguage;

import java.util.Map;

public class LispLanguageEvalRootNode extends RootNode {
    private final Map<String, RootCallTarget> functions;
    @Child
    private DirectCallNode mainCallNode = null;
    @CompilerDirectives.CompilationFinal
    private boolean isRegistered = false;

    public LispLanguageEvalRootNode(final LispLanguage lispLanguage,
                                    final RootCallTarget rootFunction,
                                    final Map<String, RootCallTarget> functions) {
        super(lispLanguage);
        this.mainCallNode = rootFunction != null ? DirectCallNode.create(rootFunction) : null;
        this.functions = functions;
    }

    @Override
    public Object execute(final VirtualFrame frame) {
        if (!this.isRegistered) {
            CompilerDirectives.transferToInterpreterAndInvalidate();
            this.lookupContextReference(LispLanguage.class).get()
                    .getFunctionRegistry().register(this.functions);
            this.isRegistered = true;
        }

        if (this.mainCallNode == null) {
            return LispLanguageNull.SINGLETON;
        } else {
            final Object[] arguments = frame.getArguments();
            for (int i = 0; i < arguments.length; i++) {
                arguments[i] = LispLanguageContext.fromForeignValue(arguments[i]);
            }
            return this.mainCallNode.call(arguments);
        }
    }
}
