package lisp.runtime;

import com.oracle.truffle.api.frame.VirtualFrame;
import lisp.LispLanguage;

public class LispLanguageUndefinedFunctionRootNode extends LispLanguageRootNode {
    public LispLanguageUndefinedFunctionRootNode(final LispLanguage language, final String name) {
        super(language, null, null, null, name);
    }

    @Override
    public Object execute(final VirtualFrame frame) {
        throw LispLanguageUndefinedNameException.undefinedFunction(null, this.getName());
    }
}
