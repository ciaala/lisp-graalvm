package lisp.runtime;

import com.oracle.truffle.api.instrumentation.InstrumentableNode;
import com.oracle.truffle.api.instrumentation.ProbeNode;
import com.oracle.truffle.api.nodes.Node;
import org.apache.commons.lang3.NotImplementedException;

public class LispLanguageWrapperNode implements InstrumentableNode.WrapperNode {

    private final LispLanguageExpressionNode lispLanguageExpressionNode;
    private final ProbeNode probe;

    public LispLanguageWrapperNode(final LispLanguageExpressionNode lispLanguageExpressionNode,
                                   final ProbeNode probe) {

        this.lispLanguageExpressionNode = lispLanguageExpressionNode;
        this.probe = probe;
    }

    @Override
    public Node getDelegateNode() {
        // TODO
        throw new NotImplementedException("Not implemented");

    }

    @Override
    public ProbeNode getProbeNode() {
        // TODO
        throw new NotImplementedException("Not implemented");

    }
}
