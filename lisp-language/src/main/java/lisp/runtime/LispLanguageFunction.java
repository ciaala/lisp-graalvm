package lisp.runtime;

import com.oracle.truffle.api.Assumption;
import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.TruffleLogger;
import com.oracle.truffle.api.interop.InteropLibrary;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.library.ExportLibrary;
import com.oracle.truffle.api.utilities.CyclicAssumption;
import lisp.LispLanguage;

import java.util.logging.Level;

@ExportLibrary(InteropLibrary.class)
public class LispLanguageFunction implements TruffleObject {
    private static final TruffleLogger LOGGER = TruffleLogger.getLogger(LispLanguage.ID, LispLanguageFunction.class);

    private final LispLanguage language;
    private final String name;
    /**
     * Manages the assumption that the {@link #callTarget} is stable. We use the utility class
     * {@link CyclicAssumption}, which automatically creates a new {@link Assumption} when the old
     * one gets invalidated.
     */
    private final CyclicAssumption callTargetStable;
    private RootCallTarget callTarget;

    public LispLanguageFunction(final LispLanguage language,
                                final String name) {

        this.language = language;
        this.name = name;
        this.callTarget = Truffle.getRuntime().createCallTarget(new LispLanguageUndefinedFunctionRootNode(language, name));
        this.callTargetStable = new CyclicAssumption(name);


    }

    protected void setCallTarget(final RootCallTarget callTarget) {
        this.callTarget = callTarget;
        /*
         * We have a new call target. Invalidate all code that speculated that the old call target
         * was stable.
         */
        LispLanguageFunction.LOGGER.log(Level.FINE, "Installed call target for: {0}", this.name);
        this.callTargetStable.invalidate();
    }

}
