package lisp.runtime;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.interop.InteropLibrary;
import com.oracle.truffle.api.interop.InvalidArrayIndexException;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.library.ExportLibrary;
import com.oracle.truffle.api.library.ExportMessage;

import java.util.HashMap;
import java.util.Map;

@ExportLibrary(InteropLibrary.class)
public class LispFunctionsObject implements TruffleObject {

    final Map<String, LispLanguageFunction> functions = new HashMap<>();

    LispFunctionsObject() {
    }

    public static boolean isInstance(final TruffleObject obj) {
        return obj instanceof LispFunctionsObject;
    }

    @ExportMessage
    boolean hasMembers() {
        return true;
    }

    @ExportMessage
    @CompilerDirectives.TruffleBoundary
    Object readMember(final String member) {
        return this.functions.get(member);
    }

    @ExportMessage
    @CompilerDirectives.TruffleBoundary
    boolean isMemberReadable(final String member) {
        return this.functions.containsKey(member);
    }

    @ExportMessage
    @CompilerDirectives.TruffleBoundary
    Object getMembers(@SuppressWarnings("unused") final boolean includeInternal) {
        return new FunctionNamesObject(this.functions.keySet().toArray());
    }

    @ExportLibrary(InteropLibrary.class)
    static final class FunctionNamesObject implements TruffleObject {

        private final Object[] names;

        FunctionNamesObject(final Object[] names) {
            this.names = names;
        }

        @ExportMessage
        boolean hasArrayElements() {
            return true;
        }

        @ExportMessage
        boolean isArrayElementReadable(final long index) {
            return index >= 0 && index < this.names.length;
        }

        @ExportMessage
        long getArraySize() {
            return this.names.length;
        }

        @ExportMessage
        Object readArrayElement(final long index) throws InvalidArrayIndexException {
            if (!this.isArrayElementReadable(index)) {
                CompilerDirectives.transferToInterpreter();
                throw InvalidArrayIndexException.create(index);
            }
            return this.names[(int) index];
        }
    }
}
