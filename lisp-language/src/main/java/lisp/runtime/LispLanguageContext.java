package lisp.runtime;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.TruffleLanguage;
import com.oracle.truffle.api.dsl.NodeFactory;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.interop.TruffleObject;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.Source;
import lisp.LispLanguage;
import lisp.runtime.builtin.LispLanguageBuiltinNode;
import lisp.runtime.builtin.PrintBuiltin;
import lisp.runtime.builtin.PrintBuiltinFactory;
import lisp.runtime.builtin.ReadBuiltin;
import lisp.runtime.builtin.ReadBuiltinFactory;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;

public class LispLanguageContext {

    public static final String STRING_NAME_LISP_LANGUAGE_BUILTIN = "LispLanguage builtin";

    /**
     * Place Holder used as Source for the Builtin Function
     */
    // TODO define in an abstract class common to the Builtin
    private static final Source BUILTIN_SOURCE = Source.newBuilder(
            LispLanguage.ID,
            StringUtils.EMPTY,
            LispLanguageContext.STRING_NAME_LISP_LANGUAGE_BUILTIN).build();

    private final LispLanguage lispLanguage;
    private final TruffleLanguage.Env env;

    private final BufferedReader input;
    private final PrintWriter output;
    private final LispLanguageFunctionRegistry functionRegistry;


    public LispLanguageContext(final LispLanguage lispLanguage,
                               final TruffleLanguage.Env env,
                               final List<NodeFactory<? extends LispLanguageBuiltinNode>> externalBuiltins) {


        this.lispLanguage = lispLanguage;
        this.env = env;

        // Context specific
        this.input = new BufferedReader(new InputStreamReader(env.in()));
        this.output = new PrintWriter(env.out(), true);
        this.functionRegistry = new LispLanguageFunctionRegistry(lispLanguage);
        // Initialize the ExternalBuiltins
        this.installBuiltins();
        for (final NodeFactory<? extends LispLanguageBuiltinNode> externalBuiltin : externalBuiltins) {
            this.installBuiltin(externalBuiltin);
        }

    }

    private static NodeInfo lookupNodeInfo(final Class<?> clazz) {
        if (clazz == null) {
            return null;
        }
        final NodeInfo info = clazz.getAnnotation(NodeInfo.class);
        if (info != null) {
            return info;
        } else {
            return LispLanguageContext.lookupNodeInfo(clazz.getSuperclass());
        }
    }

    public static Object fromForeignValue(final Object a) {
        if (a instanceof Long || a instanceof LispLanguageBigDecimalNode || a instanceof String || a instanceof Boolean) {
            return a;
        } else if (a instanceof Character) {
            return String.valueOf(a);
        } else if (a instanceof Number) {
            return LispLanguageContext.fromForeignNumber(a);
        } else if (a instanceof TruffleObject) {
            return a;
        } else if (a instanceof LispLanguageContext) {
            return a;
        }
        CompilerDirectives.transferToInterpreter();
        throw new IllegalStateException(a + " is not a Truffle value");
    }

    @CompilerDirectives.TruffleBoundary
    private static long fromForeignNumber(final Object a) {
        return ((Number) a).longValue();
    }


    public LispLanguage getLispLanguage() {
        return this.lispLanguage;
    }

    public TruffleLanguage.Env getEnv() {
        return this.env;
    }

    public void installBuiltin(final NodeFactory<? extends LispLanguageBuiltinNode> factory) {
        System.out.println("Registering Builtin " + factory.getNodeClass().toString());
        final int argumentCount = factory.getExecutionSignature().size();
        final LispLanguageExpressionNode[] argumentNodes = new LispLanguageExpressionNode[argumentCount];
        for (int index = 0; index < argumentCount; index++) {
            argumentNodes[index] = new LispLanguageReadArgumentNode(index);
        }
        /* Instantiate the builtin node. This node performs the actual functionality. */
        this.instantiateBuiltinNode(factory, argumentNodes);
    }

    private void installBuiltins() {
        this.installBuiltin(PrintBuiltinFactory.getInstance());
        this.installBuiltin(ReadBuiltinFactory.getInstance());
    }

    private void instantiateBuiltinNode(final NodeFactory<? extends LispLanguageBuiltinNode> factory,
                                        final LispLanguageExpressionNode[] argumentNodes) {
        final LispLanguageBuiltinNode builtinBodyNode = factory.createNode((Object) argumentNodes);

        final String name = LispLanguageContext.lookupNodeInfo(builtinBodyNode.getClass()).shortName();
        builtinBodyNode.addRootTag();
        final LispLanguageRootNode node = new LispLanguageRootNode(
                this.lispLanguage,
                new FrameDescriptor(),
                builtinBodyNode,
                LispLanguageContext.BUILTIN_SOURCE.createUnavailableSection(),
                name);
        this.getFunctionRegistry().register(name,
                Truffle.getRuntime().createCallTarget(node));
    }

    /**
     * Returns the default input, i.e., the source for the {@link ReadBuiltin}. To allow unit
     * testing, we do not use {@link System#in} directly.
     */
    public BufferedReader getInput() {
        return this.input;
    }

    /**
     * The default default, i.e., the output for the {@link PrintBuiltin}. To allow unit
     * testing, we do not use {@link System#out} directly.
     */
    public PrintWriter getOutput() {
        return this.output;
    }

    /**
     * Returns the registry of all functions that are currently defined.
     */
    public LispLanguageFunctionRegistry getFunctionRegistry() {
        return this.functionRegistry;
    }
}