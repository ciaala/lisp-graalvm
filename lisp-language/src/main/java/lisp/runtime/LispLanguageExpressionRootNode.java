package lisp.runtime;

import com.oracle.truffle.api.TruffleLanguage;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.RootNode;

public class LispLanguageExpressionRootNode extends RootNode {

    private final LispLanguageExpressionNode node;

    public LispLanguageExpressionRootNode(final TruffleLanguage<?> language,
                                          final FrameDescriptor frameDescriptor) {
        super(language, frameDescriptor);
        this.node = new LispLanguageExpressionNode();
    }

    @Override
    public Object execute(final VirtualFrame frame) {
        System.out.println("Calling execute on the root node");
        System.out.println("----------------------------");
        //return this.getCallTarget().call(frame.getArguments());
        final Object result = this.node.execute(frame);
        System.out.println("----------------------------");
        return result;
    }

    public void addChild(final LispLanguageExpressionNode subNode) {
        this.node.addChild(subNode);
    }

    public LispLanguageExpressionNode getBody() {
        return this.node;
    }
}
