package lisp.runtime;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.profiles.BranchProfile;

public class LispLanguageReadArgumentNode extends LispLanguageExpressionNode {
    private final int index;
    /**
     * Profiling information, collected by the interpreter, capturing whether the function was
     * called with fewer actual arguments than formal arguments.
     */
    private final BranchProfile outOfBoundsTaken = BranchProfile.create();

    public LispLanguageReadArgumentNode(final int index) {
        this.index = index;
    }

    @Override
    public Object execute(final VirtualFrame frame) {
        final Object[] args = frame.getArguments();
        if (this.index < args.length) {
            return args[this.index];
        } else {
            /* In the interpreter, record profiling information that the branch was used. */
            this.outOfBoundsTaken.enter();
            /* Use the default null value. */
            return LispLanguageNull.SINGLETON;
        }
    }
}
