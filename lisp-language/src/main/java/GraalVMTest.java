import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

public class GraalVMTest {
    public static void main(String[] args) {
        RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
        String vmVendor = runtimeMXBean.getVmName();
        if (vmVendor.contains("GraalVM")) {
            System.out.println("Running with GraalVM:\n" + vmVendor);
        } else {
            System.err.println("NOT Running with GraalVM\n" + vmVendor);
        }
    }
}
