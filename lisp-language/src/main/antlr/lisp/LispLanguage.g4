grammar LispLanguage;

@header {
package lisp;
}


lispLanguage
    : (COMMENT | sExpression)+ EOF
;
COMMENT : ';'';' (~'\n')* [\r\n];

LP : '(' ;
RP : ')' ;

WS
    : [ \t\r\n\u000C]+
    -> skip
    ;

sExpression: LP sExpression+ RP
            | LP RP
            | numericLiteral
            | stringLiteral
            | identifier
            ;

stringLiteral
    : DOUBLE_STRING
    | SINGLE_STRING
    ;

numericLiteral
    : NUMBER_TOKEN
    | DECIMAL_TOKEN
    | RATIONAL_TOKEN
    ;
identifier
    : CHARACTER_SEQUENCE
    ;

NUMBER_TOKEN: NUMERICAL_SIGN? NON_ZERO_DIGIT DIGIT_SEQ | ZERO;
DECIMAL_TOKEN: NUMERICAL_SIGN? ( ZERO | NON_ZERO_DIGIT DIGIT_SEQ)? DECIMAL;
RATIONAL_TOKEN: NUMBER_TOKEN '/' (NON_ZERO_DIGIT);

CHARACTER_SEQUENCE: [a-zA-Z_][a-zA-Z0-9_]+[#!]?;
DOUBLE_STRING : '"' ('\\' . | ~ ('\\' | '"'))* '"';
SINGLE_STRING : '\'' ('\\' . | ~('\\' | '\''))* '\'';

fragment ZERO: '0';
fragment NON_ZERO_DIGIT: [1-9];
fragment DIGIT_SEQ: [0-9]+;
fragment NUMERICAL_SIGN: '+'| '-' ;
fragment DECIMAL: '.' DIGIT_SEQ ;