;; LICENSE
;; next one is test on empty comment
;;
;; EMPTY FORMULA
()
;; One Integer
(1)
;; Two Integer
;; SIGNED_DECIMAL
(+.234)
;; ZERO
(0)
;; ZEROED DECIMAL
(0.12)
;; NEGATIVE ZERO DECIMAL
(-0.12)
;; comment
(122)
(12.2)
(-12.2)
(+12.2)
(12.0)

-112
+23
(((4/5)))
(((4/3)))